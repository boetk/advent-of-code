//============================================================================
// Name        : AdventOfCode.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include "FuelCalculator.h"
#include "Intcode.h"
using namespace std;

int main()
{
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!
	FuelCalculator f;
	int a = f.fuelCounterUpperRunner();
	Intcode ic("day2p1.txt");
	ic.run();
	return 0;
}
