/*
 * FuelCalculator.cpp
 *
 *  Created on: 10 Dec 2019
 *      Author: boetk
 */
#include "FuelCalculator.h"

FuelCalculator::FuelCalculator(){};

int FuelCalculator::fuelCounterUpper (int a) {
	return a / 3 - 2;
};

int FuelCalculator::fuelCounterUpperRunner(){
	string line;
	ifstream infile("resources/day1.txt");

	int res = 0;
	while (getline(infile, line))
	{
		int input = stoi(line);
		int tempFuel = input, fuel = 0;
		while(tempFuel > 0)
		{
			tempFuel = fuelCounterUpper(tempFuel);// process pair (a,b)
			if (tempFuel > 0) fuel += tempFuel;
		}

	    res += fuel;
	}
	cout << "Res: " << res << endl;
};
