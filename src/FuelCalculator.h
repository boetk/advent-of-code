/*
 * FuelCalculator.h
 *
 *  Created on: 10 Dec 2019
 *      Author: boetk
 */

#ifndef FUELCALCULATOR_H_
#define FUELCALCULATOR_H_

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class FuelCalculator {
public:
	FuelCalculator();
	int fuelCounterUpper (int a);
	int fuelCounterUpperRunner();
};

#endif /* FUELCALCULATOR_H_ */
