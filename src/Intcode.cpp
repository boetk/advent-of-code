/*
 * Intcode.cpp
 *
 *  Created on: 12 Dec 2019
 *      Author: boetk
 */

#include "Intcode.h"


Intcode::Intcode(){
}
Intcode::Intcode(string fileName) {
	loadFile(fileName);
}

Intcode::~Intcode() {
	// TODO Auto-generated destructor stub
}

void Intcode::printElements()
{
	for(int x : elements){
		printf("%d, ", x);
	}
	printf("\n");
}

void Intcode::run()
{
	int index = 0;
	while(index <= elements.size() || elements[index] == 99){
		int firstElement = elements[index];
		int res;
		if(firstElement == 1){
			res = elements[elements[index+1]] + elements[elements[index+2]];
		}else if (firstElement == 2 ){
			res = elements[elements[index+1]] * elements[elements[index+2]];
		}
		elements[elements[index+3]] = res;
		index += 4;
		printElements();
	}
}

void Intcode::loadFile(string name){
	string line;
	ifstream infile("resources/"+ name);

	int res = 0;
	while (getline(infile, line))
	{
		string temp;
		stringstream ss(line);
		while(getline(ss, temp, ',')){
			elements.push_back(stoi(temp));
		}
	}
	printElements();
};

void Intcode::loadString(string input){
	string temp;
    stringstream ss(input);
	while(getline(ss, temp, ',')){
		elements.push_back(stoi(temp));
	}
	printElements();
}
