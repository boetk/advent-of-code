/*
 * Intcode.h
 *
 *  Created on: 12 Dec 2019
 *      Author: boetk
 */

#ifndef INTCODE_H_
#define INTCODE_H_

#include <string>;
#include <sstream>;
#include <iostream>;
#include <vector>;
#include <fstream>

using namespace std;

class Intcode {
public:
	Intcode();
	Intcode(string);
	virtual ~Intcode();
	void printElements();
	void run();
	void loadFile(string);
	void loadString(string);
	vector<int> elements;


};

#endif /* INTCODE_H_ */
